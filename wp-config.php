<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Mm=qQKU[C24ZY</L8UM9E/c8?NB9rFeE9kL;WS|>`^a|E{[;PKee!Fp*tIc0A;$&');
define('SECURE_AUTH_KEY',  ']}!us|-O#*z<+HuAc:7Z:^tO4tMy6@@E_?|+x?oq[1+6*YPTR_9nv@X>:.9<$G,u');
define('LOGGED_IN_KEY',    'VaLmCW|Z;hqQ:!OCx.6|Bod?7p+G]}N/tzs,Mtz7<(UJ^M_)!LPb|ujXsp#peM[N');
define('NONCE_KEY',        'm^fWxW,r`0_=|>/]fMZ3{=RH+m_&W.fzT,qxH$8GK%c8{nB%l%<,EXLEKN}]lZv{');
define('AUTH_SALT',        'JB67;iG&c{@0-&|>!K{|VR*K]>XZb@Fb :v[O{saEOt4aR][BxR`L(=h+@+2`bwy');
define('SECURE_AUTH_SALT', '<Z2ts$m~Fu(J|]r9y-n,uRO8m#l!Ppu@yO!:0~OfmZTW3<T2I}Y@D$ 2H`-fE7.p');
define('LOGGED_IN_SALT',   '9*g^LKT%vn{%c(LDo=#%:_{nUg`&VA)et{cM6k^p$1@ptlRW85*[&}-)pJ?m4h6R');
define('NONCE_SALT',       'iq`CVHJ@i7VDWIdGx`PQ4-N)CV. 725h<DgyGu]D298g[#nV4SWW?<ACfwVE=K,b');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
