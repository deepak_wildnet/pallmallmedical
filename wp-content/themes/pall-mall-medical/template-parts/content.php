<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package pall_mall_medical
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php
		if ( is_single() ) :
			the_title( '<h1 class="entry-title">', '</h1>' );
		else :
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif;

		if ( 'post' === get_post_type() ) : ?>
		<div class="entry-meta">
			<?php pall_mall_medical_posted_on(); ?>
		</div><!-- .entry-meta -->
		<?php
		endif; ?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php
			the_content( sprintf(
				/* translators: %s: Name of current post. */
				wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'pall-mall-medical' ), array( 'span' => array( 'class' => array() ) ) ),
				the_title( '<span class="screen-reader-text">"', '"</span>', false )
			) );

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'pall-mall-medical' ),
				'after'  => '</div>',
			) );
		?>
		<?php
		$value = get_post_meta( $post->ID, 'accordian_page_data', true );
    	$data = unserialize($value);
    	if(!empty($data)){
    		foreach($data as $value){ ?>
    		<button class="accordion"><?php echo $value['title'];?></button>
			<div class="panel">
			  <p><?php echo $value['description']; ?></p>
			</div>
    	<?php } } ?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php pall_mall_medical_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
