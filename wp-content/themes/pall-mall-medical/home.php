<?php
/*
 * Call the header 
*/
echo get_header(); ?>

<!-- Header Carousel -->
<div class="flexslider">
      <ul class="slides">
        <?php 
        $args = array('post_type' =>'slider','posts_per_page'=>3 );
        $query = new WP_Query($args);
        while($query->have_posts()) : $query->the_post();
        ?>
        <li>
          <?php the_post_thumbnail(array(1500)); ?>
              <div class="overlay">&nbsp;</div>
          <div class="carousel-caption">
          
            <h2><?php the_title(); ?></h2>
            
            <p><?php the_content(); ?></p>
          
          </div>
        </li>
      <?php endwhile; ?>
      </ul>
    </div>
<!-- <header id="myCarousel" class="carousel slide"> 
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
  </ol>
  

  <div class="carousel-inner">
    <?php 

    $args = array('post_type' =>'slider','posts_per_page'=>3 );

    $query = new WP_Query($args);
    $i = 0;
    while($query->have_posts()) : $query->the_post();
    ?>

    <div class="item <?php echo ($i == 0 ) ? 'active' : ''; ?>">
      <div class="fill"><?php the_post_thumbnail(array(1500)); ?></div>
      <div class="carousel-caption">
        <h2><?php the_title(); ?></h2>
        <?php the_content(); ?>
      </div>
    </div>
    <?php $i++; endwhile; ?>
  </div>
 </header> -->
 <div class="clearfix"></div>

<div class="container"> 
  
  <!-- Make an Appointment Section -->
  <div class="row appointment">
    <div class="col-md-12">
      <h1> Make an appointment </h1>
      <form>
        <div class="col-md-4">
          <div class="form-group">
            <div class="icon-addon">
              <input type="text" placeholder="First Name" class="form-control" id="first_name">
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <div class="icon-addon">
              <input type="text" placeholder="Last Name" class="form-control" id="last_name">
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <div class="icon-addon">
              <input type="text" placeholder="Email" class="form-control" id="email">
            </div>
          </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-4">
          <div class="form-group">
            <div class="icon-addon">
              <input type="text" placeholder="Date" class="form-control" id="date">
            </div>
          </div>
        </div>
        <div class="col-md-3">
          <select name="">
            <option>Department</option>
            <option>Department</option>
          </select>
        </div>
        <div class="col-md-3">
          <select name="">
            <option>Location</option>
            <option>Location</option>
          </select>
        </div>
       
       <div class="col-md-2"> <button type="submit" class="btn btn-default">Submit</button></div>
      </form>
    </div>
  </div>
  <!-- /.row --> 
</div>


<div class="clearfix"></div>
<!-- Services Section -->
<div class="services">
  <div class="container">
    <div class="col-md-4 col-sm-4 padt100">
      <h2>Our Services</h2>
      <p>Pall Mall Medical provides high quality privat healthcare services to self-paying and private medical insured patients in the comfort of privat facilities with a high standard of care. </p>
    </div>
    <div class="col-md-8 col-sm-8">
    <?php 
    	$args = array('post_type'=>'service','posts_per_page'=>3,'cat'=>'-5');
    	$query = new WP_Query($args);
    	if($query->have_posts()) :
    		while($query->have_posts()) : $query->the_post();
    ?>
      <div class="box">
      	<a href="<?php the_permalink(); ?>"> <?php the_post_thumbnail(); ?>
        	<div class="box_icon">
          		<div class="ic">
          			<img src="<?php the_field('icon'); ?>" >
          		</div>
          		<div class="clearfix"></div>
        	</div>
        	<h2><?php the_title(); ?></h2>
        <?php the_excerpt(); ?>
        </a>
      </div>
      <?php endwhile; endif; ?>
    </div>
    <div class="clearfix"></div>
  </div>
  <div class="clearfix"></div>
</div>
<div class="clearfix"></div>

<!-- Finance and Specialist Section -->
<div class="finance">
  <div class="container">
  <?php 
    	$args = array('post_type'=>'service','posts_per_page'=>2,'category'=>5);
    	$query = new WP_Query($args);
    	$i = 0;
    	if($query->have_posts()) :
    		while($query->have_posts()) : $query->the_post();
    ?>
	    <div class="col-md-6 col-sm-6 parent">
	      <div class="<?php echo ($i == 0) ? 'green_bg' : 'green_bg1'; ?>">
	        <?php the_content(); ?>
	      </div>
	      <div class="box_img"> <img src="<?php echo get_template_directory_uri().'/images/finance.jpg'; ?>" class="img-responsive">
	        <h2><?php the_title(); ?></h2>
	      </div>
	    </div>
	<?php $i++; endwhile; endif; ?>
    <div class="clearfix"></div>
  </div>
  <div class="clearfix"></div>
</div>
<div class="clearfix"></div>

<!-- Blog Section -->
<div class="blog">
  <div class="container">
    <div class="col-md-6 ask">
      <h2>Ask Dr Jenna Burton</h2>
      <form>
        <div class="form-group">
          <label> Your name (you dont have to use your real name)</label>
          <div class="icon-addon">
            <input type="text" placeholder="Name..." class="form-control" id="name">
          </div>
        </div>
        <div class="form-group">
          <label>Your email address (won't be published)</label>
          <div class="icon-addon">
            <input type="text" placeholder="Email Address..." class="form-control" id="email2">
          </div>
        </div>
        <div class="form-group">
          <label>Your question</label>
          <div class="icon-addon">
            <textarea name="" cols="4" rows="4" class="form-control" id="ques" placeholder="Question"></textarea>
            <button type="submit" class="btn btn-default">Submit</button>
          </div>
        </div>
        <div class="clearfix"></div>
      </form>
    </div>
    <div class="col-md-6 blogs">
      <h2>Blog Updates <span><img src="<?php echo get_template_directory_uri().'/images/arrow.png'; ?>" width="24" height="18"></span></h2>
      <?php
      $args = array("posts_per_page"=>2);
      $the_query = new WP_Query($args);
     	if ( $the_query->have_posts() ) :
			while ( $the_query->have_posts() ) :
				$the_query->the_post(); 
		  ?>

	      <div class="blog_list">
	        <div class="col-md-5 col-sm-5 padno">
	        	<!-- <img src="" class="img-responsive"> -->
	        	<?php the_post_thumbnail(array(226,259)); ?>
	        </div>
	        <div class="col-md-7 col-sm-7">
	          <h3><a href="<?php echo the_permalink(); ?>"><?php the_title(); ?></a></h3>
	          <small>By <?php the_author(); ?> / <?php echo get_the_date(); ?> / <?php $comments = wp_count_comments($post->ID); echo $comments->approved; ?> Comments</small>
	          <?php the_excerpt(); ?>
	        </div>
	      </div>
	      <div class="clearfix"></div>
      <?php endwhile; endif;?>
    </div>
    <div class="clearfix"></div>
    <div class="clearfix"></div>
  </div>
  <div class="clearfix"></div>
</div>
<div class="clearfix"></div>

<!-- Testimonial Section -->
<div>
   <div class=""><img src="<?php echo get_template_directory_uri().'/images/bordertop.png'; ?>" style="width:100%;  position:fixed top;"></div>
  <div class="bg">
 
    <div class="container">
      <div class="col-md-12 padrnomob" data-wow-delay="0.2s">
        <h2>What our patients saying</h2>
        <h4>Patient Says</h4>
        <div class="carousel slide" data-ride="carousel" id="quote-carousel"> 
          
          <!-- Carousel Slides / Quotes -->
          <div class="carousel-inner text-center"> 
            
            <!-- Quote 1 -->
            <?php 

            $args = array('post_type' =>'testimonial','posts_per_page'=>3 );
		    $query = new WP_Query($args);
		    $i = 0;
		    while($query->have_posts()) : $query->the_post();
		    ?>
            <div class="item <?php echo ($i==0) ? 'active' : ''; ?>">
              <blockquote>
                <div class="row">
                  <div class="col-md-8">
                    <?php the_content(); ?>
                    <div class="clearfix"></div>
                    <h5><?php the_title(); ?></h5>
                    <h5><?php echo "Project Manager"; ?></h5>
                  </div>
                </div>
              </blockquote>
            </div>
            <?php $i++; endwhile; ?>
            
          </div>
          <ol class="carousel-indicators">
    <li data-target="#quote-carousel" data-slide-to="0" class="active"></li>
    <li data-target="#quote-carousel" data-slide-to="1" class=""></li>
    <li data-target="#quote-carousel" data-slide-to="2" class=""></li>
  </ol>
        </div>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>
  <div class="clearfix"></div>
</div>
<div class="clearfix"></div>
<!-- /.row --> 
<!-- client logo Section -->
<div class="client">
  <div class="container">
    <div class="col-md-12">
      <?php  echo do_shortcode('[wpaft_logo_slider]'); ?>
      
    </div>
    <div class="clearfix"></div>
    <div class="clearfix"></div>
  </div>
  <div class="clearfix"></div>
</div>
<div class="clearfix"></div>

<?php 

echo get_footer(); ?>