<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package pall_mall_medical
 */

?>
<!-- Footer --> 

<!-- /.container -->
<footer>
  <div class="container">
    <div class="row">
      <div class="col-md-4">
        <?php

			if(is_active_sidebar('footer-sidebar-1')){
				dynamic_sidebar('footer-sidebar-1');
			}
		?>
      </div>
      <div class="col-md-4">
        <?php
			if(is_active_sidebar('footer-sidebar-2')){
				dynamic_sidebar('footer-sidebar-2');
			}
			?>
      </div>
      <div class="col-md-4">
        <h3 class="text-right">Subscribe to our newsletter</h3>
        <form class="form-inline newsletter" role="search">
          <div class="col-md-9 padno">
            <div class="form-group">
              <input type="text" class="form-control" placeholder="Enter Email...">
            </div>
          </div>
          <div class="col-md-3 padno">
            <button type="submit" class="btn btn-primary">Subscribe</button>
          </div>
        </form>
        <div class="clearfix"></div>
        <h3 class="text-right">Connect With Us</h3>
        <ul class="social">
          <li><a href="https://www.facebook.com/pallmallmedicaluk" target="_blank"><i class="fa fa-facebook"></i></a></li>
          <li><a href="https://twitter.com/pallmallmedical" target="_blank"><i class="fa fa-twitter"></i></a></li>
          <li><a href="https://plus.google.com/+pallmallmedical" target="_blank"><i class="fa fa-google-plus"></i></a></li>
          <li><a href="http://uk.linkedin.com/in/pallmallmedical" target="_blank"><i class="fa fa-linkedin"></i></a></li>
        </ul>
      </div>
    </div>
  </div>
</footer>
<div class="footer-bottom">
  <div class="container"> Company no. 08502048 © Pall Mall Medical 2016. All rights reserved. </div>
  <div class="clearfix"></div>
</div>
<?php wp_footer(); ?>

<!-- Script to Activate the Carousel --> 
<script>
    
	$(document).ready(function(){
    $(".dropdown").hover(            
        function() {
            $('.dropdown-menu', this).stop( true, true ).slideDown("fast");
            $(this).toggleClass('open');        
        },
        function() {
            $('.dropdown-menu', this).stop( true, true ).slideUp("fast");
            $(this).toggleClass('open');       
        }
    );

    $('.flexslider').flexslider({
        animation: "fade",
        auto: true,
    });

    $('.carousel').carousel({
        interval: 5000 //changes the speed
    });

    $('#Carousel').carousel({
        interval: 5000
    });

    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].onclick = function(){
            this.classList.toggle("active");
            this.nextElementSibling.classList.toggle("show");
      }
    }

});
</script>

	<!-- </div>

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="site-info">
			<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'pall-mall-medical' ) ); ?>"><?php printf( esc_html__( 'Proudly powered by %s', 'pall-mall-medical' ), 'WordPress' ); ?></a>
			<span class="sep"> | </span>
			<?php printf( esc_html__( 'Theme: %1$s by %2$s.', 'pall-mall-medical' ), 'pall-mall-medical', '<a href="https://automattic.com/" rel="designer">Underscores.me</a>' ); ?>
		</div>
	</footer>
</div> -->



</body>
</html>
