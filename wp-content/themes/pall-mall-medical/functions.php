<?php
/**
 * pall mall medical functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package pall_mall_medical
 */

if ( ! function_exists( 'pall_mall_medical_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function pall_mall_medical_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on pall mall medical, use a find and replace
	 * to change 'pall-mall-medical' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'pall-mall-medical', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'menu-1' => esc_html__( 'Primary', 'pall-mall-medical' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'pall_mall_medical_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );
}
endif;
add_action( 'after_setup_theme', 'pall_mall_medical_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function pall_mall_medical_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'pall_mall_medical_content_width', 640 );
}
add_action( 'after_setup_theme', 'pall_mall_medical_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function pall_mall_medical_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'pall-mall-medical' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'pall-mall-medical' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name' => 'Footer Sidebar 1',
		'id' => 'footer-sidebar-1',
		'description' => 'Appears in the footer area',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
		) );
		register_sidebar( array(
		'name' => 'Footer Sidebar 2',
		'id' => 'footer-sidebar-2',
		'description' => 'Appears in the footer area',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
		) );
		register_sidebar( array(
		'name' => 'Footer Sidebar 3',
		'id' => 'footer-sidebar-3',
		'description' => 'Appears in the footer area',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
		) );
}
add_action( 'widgets_init', 'pall_mall_medical_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function pall_mall_medical_scripts() {
	
	wp_enqueue_style( 'pall-mall-medical-bootstrap', get_template_directory_uri().'/css/bootstrap.min.css' );

	wp_enqueue_style( 'pall-mall-medical-style', get_stylesheet_uri() );

	wp_enqueue_style( 'pall-mall-medical-responsive', get_template_directory_uri().'/css/responsive.css' );
	wp_enqueue_style( 'pall-mall-medical-flexslider', get_template_directory_uri().'/css/flexslider.css' );
	
	wp_enqueue_style( 'pall-mall-medical-fonts', get_template_directory_uri().'/css/font-awesome.min.css' );

	wp_enqueue_script( 'pall-mall-medical-jquery', get_template_directory_uri() . '/js/jquery.js', array(), '20151215', true );

	wp_enqueue_script( 'pall-mall-medical-bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array(), '20151215', true );
	wp_enqueue_script( 'pall-mall-medical-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );
	wp_enqueue_script( 'pall-mall-medical-flexslider-js', get_template_directory_uri() . '/js/jquery.flexslider.js', array(), '20151215', true );
	
	wp_enqueue_script( 'pall-mall-medical-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'pall_mall_medical_scripts' );

function pall_mall_medical_admin_scripts( $hook ) {
    // if ('edit.php' != $hook) {
    //     return;
    // }
    wp_enqueue_script( 'wp-admin-js', get_template_directory_uri() . '/js/wp-admin.js' );
    wp_enqueue_style( 'wp-admin-css', get_template_directory_uri() . '/css/wp-admin.css' );
}

add_action('admin_enqueue_scripts', 'pall_mall_medical_admin_scripts');

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Set the excerpt Length 
*/
function pall_mall_excerpt_length( $length ) {
    return 20;
}
add_filter( 'excerpt_length', 'pall_mall_excerpt_length', 999 );

/**
 * Set the excerpt read more 
*/
function wpdocs_excerpt_more( $more ) {
	return '...';
}
add_filter( 'excerpt_more', 'wpdocs_excerpt_more' );

/**
 * If Featured image not found show default image
*/
add_filter( 'post_thumbnail_html', 'my_post_thumbnail_html' );

function my_post_thumbnail_html( $html ) {

	if ( empty( $html ) )
		$html = '<img src="' . trailingslashit( get_stylesheet_directory_uri() ) . 'images/default-thumbnail.jpg' . '" alt="" width="226" height="159" />';

	return $html;
}

/*
 * Add Meta Field in page post type
*/
function pall_mall_medical_add_meta_box() {

    $screens = array( 'post', 'page' );

    foreach ( $screens as $screen ) {

        add_meta_box(
            'mymetabox',
            __( 'Accordian Data', 'pall_mall_medical_text' ),
            'pall_mall_medical_meta_box_callback',
            $screen
        );
    }
}
add_action( 'add_meta_boxes', 'pall_mall_medical_add_meta_box' );

/**
 * Prints the box content.
 *
 * @param WP_Post $post The object for the current post/page.
 */
function pall_mall_medical_meta_box_callback( $post ) {

    // Add a nonce field so we can check for it later.
    wp_nonce_field( 'pall_mall_data_save_meta_box_data', 'myplugin_meta_box_nonce' );

    /*
     * Use get_post_meta() to retrieve an existing value
     * from the database and use the value for the form.
     */

    $value = get_post_meta( $post->ID, 'accordian_page_data', true );
    $data = unserialize($value);
    if(!empty($data)){
    	$count = count($data);
    	$i = 0;
    	foreach ($data as $key => $value) {
    		$i++;
    		echo '<div class="parent_class">';
			    echo '<div class="form-group">';
			    	echo '<label for="title" class="pall_mall_medical_tite">';
			    	_e( 'Title', 'pall_mall_medical_text' );
			    	echo '</label> ';
			    
			    	echo '<input type="text" name="data_title[]" class="pall_mall_medical_text" value="' . esc_attr( $value['title'] ) . '" size="25" />';
			    echo '</div>';

			    echo '<div class="form-group">';
				    echo '<label for="description" class="pall_mall_medical_tite">';
				    _e( 'Description', 'pall_mall_medical_description' );
				    echo '</label> ';
				    echo '<textarea  class="tinymce-enabled" name="data_description[]" class="pall_mall_medical_text">' . esc_attr( $value['description'] ) . '</textarea>';
				   	echo ($i == $count) ? '<a class="addnewmore">+</a>' : '<a class="addnewmore">-</a>';
			    echo '</div>';
		    echo '</div>';
		}
    }else{

    	echo '<div class="parent_class">';
		    echo '<div class="form-group">';
		    	echo '<label for="title" class="pall_mall_medical_tite">';
		    	_e( 'Title', 'pall_mall_medical_text' );
		    	echo '</label> ';
		    
		    	echo '<input type="text" name="data_title[]" class="pall_mall_medical_text" value="" size="25" />';
		    echo '</div>';

		    echo '<div class="form-group">';
			    echo '<label for="description" class="pall_mall_medical_tite">';
			    _e( 'Description', 'pall_mall_medical_description' );
			    echo '</label> ';
			    echo '<textarea  class="tinymce-enabled" name="data_description[]" class="pall_mall_medical_text"></textarea>';
			   	echo '<a class="addnewmore">+</a>';
		    echo '</div>';
    	echo '</div>';
    }
    
}

/**
 * When the post is saved, saves our custom data.
 *
 * @param int $post_id The ID of the post being saved.
 */
function pall_mall_data_save_meta_box_data( $post_id ) {

    /*
     * We need to verify this came from our screen and with proper authorization,
     * because the save_post action can be triggered at other times.
     */

    // Check if our nonce is set.
    if ( ! isset( $_POST['myplugin_meta_box_nonce'] ) ) {
        return;
    }

    // Verify that the nonce is valid.
    if ( ! wp_verify_nonce( $_POST['myplugin_meta_box_nonce'], 'pall_mall_data_save_meta_box_data' ) ) {
        return;
    }

    // If this is an autosave, our form has not been submitted, so we don't want to do anything.
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
        return;
    }

    // Check the user's permissions.
    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {

        if ( ! current_user_can( 'edit_page', $post_id ) ) {
            return;
        }

    } else {

        if ( ! current_user_can( 'edit_post', $post_id ) ) {
            return;
        }
    }

    /* OK, it's safe for us to save the data now. */
    
    // Make sure that it is set.
    // if ( ! isset( $_POST['data_title'] ) ) {
    //     return;
    // }

    // Sanitize user input.
    $data_text = $_POST['data_title'];
    $data_description = $_POST['data_description'];
    
    foreach ($data_text as $key => $value) {
    	$merged_array[$key] = array('title'=>$value,'description'=>$data_description[$key]);
    }
    
    $serialize_data = serialize($merged_array);
    // Update the meta field in the database.
    update_post_meta( $post_id, 'accordian_page_data', $serialize_data );
}
add_action( 'save_post', 'pall_mall_data_save_meta_box_data' ); 
