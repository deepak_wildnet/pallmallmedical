<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package pall_mall_medical
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head(); ?>
</head>

<body <?php //body_class(); ?>>
<header class="header">
	<div class="top-header">
	  <div class="container">
	    <div class="col-lg-5 col-md-6 col-sm-6 col-xs-12">
	      <p><?php echo get_option_tree('have_any__question'); ?> </p>
	    </div>
	    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 text-right padrno change">
			<ul class="social">
			  <li><a href="#"><i class="fa fa-facebook"></i></a></li>
			  <li><a href="#"><i class="fa fa-twitter"></i></a></li>
			  <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
			  <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
			</ul>
			<?php //echo do_shortcode('[google-translator]'); ?>
		</div>
	  </div>
	  <div class="clearfix"></div>
	</div>
	<div class="clearfix"></div>
		
	<div class="container"> 
	    <!-- Brand and toggle get grouped for better mobile display -->
	    <div class="navbar-header">
	      <div class="col-md-3"> 
	      	<a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>">
	      		<img src="<?php echo get_option_tree('logo') ?>" alt="logo">
		    </a>
		   </div>
	      <div class="padrno psdlnomob">
			
	        <div class="borright">
	          <p>Manchester</p>
	          <h3><?php echo get_option_tree('manchester'); ?></h3>
	        </div>
	        <div class="borright">
	          <p>Liverpool</p>
	          <h3><?php echo get_option_tree('liverpool'); ?></h3>
	        </div>
	        <div class="borright">		
	          <p>Newton-le-Willows</p>
	          <h3><?php echo get_option_tree('newton_le_willows'); ?></h3>
	        </div>
	        <div class="form-one">
			
				<form class="navbar-form navbar-left" role="search">
					<div class="form-group">
						<input type="text" class="form-control dip" placeholder="Search...">
						
						<button type="button" class="btn btn-default"><i class="fa fa-search"></i></button>
					</div>
					
					<div class="iwantto">
					<ul class="iwantto-options">
						<li><a href="/our-services">Find a service</a></li>
						<li><a href="/consultant-directory">Find a consultant</a></li>
						<li><a href="/patients-and-visitors">Plan my visit</a></li>
						<li><a href="/our-locations">Get directions to hospital</a></li>
						<li><a href="/contact-us">Contact the hospital</a></li>
					</ul>
					</div>
					
				</form>		
			</div>			
		  </div>
	      <div class="clearfix"></div>
	    </div>
    	<div class="clearfix"></div>
    </div>

<!-- Navigation -->

<nav class="navbar navbar-inverse" role="navigation">
  <div class="container menu_padd">
  	<div class="menu">
  		<?php wp_nav_menu( array( 'theme_location' => 'menu-1' ) ); ?>	
  	</div>
  </div>
</nav>
</header>
<div class="clearfix"></div>

 <!-- <div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'pall-mall-medical' ); ?></a>

	<header id="masthead" class="site-header" role="banner">
		<div class="site-branding">
			<?php
			if ( is_front_page() && is_home() ) : ?>
				<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
			<?php else : ?>
				<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
			<?php
			endif;

			$description = get_bloginfo( 'description', 'display' );
			if ( $description || is_customize_preview() ) : ?>
				<p class="site-description"><?php echo $description; /* WPCS: xss ok. */ ?></p>
			<?php
			endif; ?>
		</div>

		<nav id="site-navigation" class="main-navigation" role="navigation">
			<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Primary Menu', 'pall-mall-medical' ); ?></button>
			<?php wp_nav_menu( array( 'theme_location' => 'menu-1' ) ); ?>
		</nav>
	</header>

	<div id="content" class="site-content">  -->
